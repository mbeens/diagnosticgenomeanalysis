/*
First, give a short overview of what we have in our database. After inserting the data you showed an overview of the number of rows per table, now, create some queries to report on:

    the number of variants per chromosome and
    the number of variants per gene
*/

select * from variants